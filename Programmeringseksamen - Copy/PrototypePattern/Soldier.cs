﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern
{
    class Soldier : IClonable<Soldier>
    {
        private long fibbonacciNumber;
        public long Damage
        {
            get
            {
                return fibbonacciNumber;
            }
        }
        public Soldier(int damageIndex)
        {
            fibbonacciNumber = GetFibbonacciNumber(damageIndex);
        }
        private Soldier(long fibbonacciNumber)
        {
            this.fibbonacciNumber = fibbonacciNumber;
        }
        private static long GetFibbonacciNumber(int index)
        {
            int prev = 1;
            int fibbonacci = 1;
            for (int i = 2; i < index; i++)
            {
                int current = fibbonacci;
                fibbonacci += prev;
                prev = current;
            }
            return fibbonacci;
        }

        public Soldier Clone()
        {
            return new Soldier(fibbonacciNumber);
        }
    }
}
