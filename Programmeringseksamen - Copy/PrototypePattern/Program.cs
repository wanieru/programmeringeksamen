﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrototypePattern
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime beforeFirst = DateTime.Now;
            Soldier soldierPrototype = new Soldier(200000);
            DateTime afterPrototype = DateTime.Now;
            for (int i = 0; i < 100000; i++)
            {
                Soldier soldierClone = soldierPrototype.Clone();
            }
            DateTime afterClones = DateTime.Now;

            Console.WriteLine("Soldier damage (200.000 fibbonacci): " + soldierPrototype.Damage);
            Console.WriteLine("Initial prototype: " + (afterPrototype - beforeFirst).TotalMilliseconds + " ms");
            Console.WriteLine("100.000 clones: " + (afterClones - afterPrototype).TotalMilliseconds + " ms");

            Console.ReadLine();
        }
    }
}
