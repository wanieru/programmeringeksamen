﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Graphs.Graph
{
    /// <summary>
    /// Reperesents a graph of nodes and edges with some content of type T stored on each node.
    /// </summary>
    /// <typeparam name="T">The type of content stored on each node</typeparam>
    public class Graph<T>
    {
        private List<Node<T>> nodes;
        /// <summary>
        /// The nodes stored in this graph
        /// </summary>
        public List<Node<T>> Nodes
        {
            get
            {
                return nodes;
            }
        }

        /// <summary>
        /// Constructs a new graph with no nodes
        /// </summary>
        public Graph()
        {
            nodes = new List<Node<T>>();
        }
        /// <summary>
        /// Constructs a new graph with the specified nodes
        /// </summary>
        /// <param name="nodes">The nodes to add immediately</param>
        public Graph(params Node<T>[] nodes) : this()
        {
            this.nodes.AddRange(nodes);
        }

        /// <summary>
        /// Creates a new node on the graph with the specified content
        /// </summary>
        /// <param name="content">The content to add</param>
        /// <returns>The new node</returns>
        public Node<T> AddNode(T content)
        {
            return AddNode(content, false, 0f);
        }
        /// <summary>
        /// Adds a new node and immediately adds one-way edges (1) to the specified nodes
        /// </summary>
        /// <param name="content">The content of the node</param>
        /// <param name="edges">All the other nodes to make a one-way edge to</param>
        /// <returns>The new node</returns>
        public Node<T> AddNode(T content, params Node<T>[] edges)
        {
            return AddNode(content, false, 1f, edges);
        }
        /// <summary>
        /// Adds a new node and immediately adds edges to the specified nodes.
        /// </summary>
        /// <param name="content">The content of the node.</param>
        /// <param name="bidirectional">Whether or not the edges added to the new node are bidirectional</param>
        /// <param name="edges">The nodes to make edges to</param>
        /// <returns>The new node</returns>
        public Node<T> AddNode(T content, bool bidirectional, double cost, params Node<T>[] edges)
        {
            if (GetNode(content) != null)
                throw new NodeAlreadyExistsException<T>(GetNode(content));
            Node<T> newNode = new Node<T>(content);
            nodes.Add(newNode);

            foreach (Node<T> node in edges)
            {
                AddEdge(newNode, node, bidirectional, cost);
            }

            return newNode;
        }

        /// <summary>
        /// Adds a new edge between two nodes.
        /// </summary>
        /// <param name="from">The first node to make the edge from.</param>
        /// <param name="to">The node to connect the edge to.</param>
        /// <param name="bidirectional">If true, another edge is added in the other direction.</param>
        public void AddEdge(Node<T> from, Node<T> to, bool bidirectional, double cost)
        {
            from.AddEdge(to, cost);
            if (bidirectional)
            {
                to.AddEdge(from, cost);
            }
        }

        /// <summary>
        /// Add multiples edges from one node to multiple nodes.
        /// </summary>
        /// <param name="from">The node all edges connects to</param>
        /// <param name="bidirectional">Whether each node should be bidirational</param>
        /// <param name="toNodes">The nodes which the from node should connect to.</param>
        public void AddEdges(Node<T> from, bool bidirectional, double cost, params Node<T>[] toNodes)
        {
            foreach (var to in toNodes)
            {
                AddEdge(from, to, bidirectional, cost);
            }
        }

        /// <summary>
        /// Returns the node with the specified content.
        /// </summary>
        /// <param name="content">The content to find a node with.</param>
        /// <returns>The node with the content. Null if no node is found.</returns>
        public Node<T> GetNode(T content)
        {
            return nodes.FirstOrDefault(n => n.Content.Equals(content)); //We have to use "Equals" because you can't use the == operator on generic types
        }
    }

    public class NodeAlreadyExistsException<T> : Exception
    {
        public readonly Node<T> node;
        public NodeAlreadyExistsException(Node<T> node)
        {
            this.node = node;
        }
    }
    public class SelfReferencingNodeException<T> : Exception
    {
        private Node<T> node;
        public SelfReferencingNodeException(Node<T> node)
        {
            this.node = node;
        }
    }
}