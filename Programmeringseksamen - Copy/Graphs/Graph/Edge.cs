﻿using System.Collections;
using System.Collections.Generic;

namespace Graphs.Graph
{
    /// <summary>
    /// An edge from one node to another.
    /// </summary>
    /// <typeparam name="T">The type of the content of the nodes.</typeparam>
    public class Edge<T>
    {
        /// <summary>
        /// The from node.
        /// </summary>
        public Node<T> FromNode { get; private set; }
        /// <summary>
        /// The to node.
        /// </summary>
        public Node<T> ToNode { get; private set; }

        public double Cost{ get; private set; }

        /// <summary>
        /// Constructs a new edge from the specified node to the other node.
        /// </summary>
        /// <param name="from">The node connecting from.</param>
        /// <param name="to">The node connecting to.</param>
        public Edge(Node<T> from, Node<T> to, double cost)
        {
            this.FromNode = from;
            this.ToNode = to;
            this.Cost = cost;
        }

        public override string ToString()
        {
            return "Edge: " + FromNode.ToString() + " -> " + ToNode.ToString()+ " ("+Cost+")";
        }

    }
}