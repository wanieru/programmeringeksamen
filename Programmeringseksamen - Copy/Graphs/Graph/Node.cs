﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Graphs.Graph
{
    /// <summary>
    /// A node in the graph
    /// </summary>
    /// <typeparam name="T">The content type of the node</typeparam>
    public class Node<T>
    {
        /// <summary>
        /// The content of the node, which it represents.
        /// </summary>
        public T Content { get; private set; }
        private List<Edge<T>> edges;

        /// <summary>
        /// The edges connecting this node to another node.
        /// </summary>
        public List<Edge<T>> Edges { get { return edges; } }

        /// <summary>
        /// Constructs a new node.
        /// </summary>
        /// <param name="content">The content this node represents.</param>
        public Node(T content)
        {
            this.Content = content;
            edges = new List<Edge<T>>();
        }

        /// <summary>
        /// Adds an edge to this node.
        /// </summary>
        /// <param name="node">The other node to connect to.</param>
        public void AddEdge(Node<T> node, double cost)
        {
            if (node == this)
            {
                throw new SelfReferencingNodeException<T>(this);
            }
            if (edges.Any(e => e.ToNode == node))
            {
                throw new NodeAlreadyExistsException<T>(node);
            }
            edges.Add(new Edge<T>(this, node, cost));
        }

        /// <summary>
        /// Check if this node has an edge to the specified node.
        /// </summary>The node to check against.</param>
        /// <returns>True if this node has an edge to the specified node.</returns>
        public bool HasEdgeTo(Node<T> node)
        {
            return edges.Any(e => e.ToNode == node);
        }

        public override string ToString()
        {
            return "Node: " + Content.ToString();
        }
    }
}