﻿using Graphs.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphs.Algorithm.Astar
{
    /// <summary>
    /// Represents a node to use internally in the astar algorithm
    /// </summary>
    /// <typeparam name="T">The type of node</typeparam>
    internal class AstarNode<T> : IComparable<AstarNode<T>> where T : IPathfindingNode<T>
    {
        public Node<T> node;
        public AstarNode<T> parent;
        public double gScore; //Only my distance to my parent
        public double CombinedGScore { get { return parent == null ? gScore : parent.CombinedGScore + gScore; } }
        public double hScore;
        public double FScore { get { return CombinedGScore + hScore; } }
        public AstarNode(Node<T> node)
        {
            this.node = node;
            parent = null;
            gScore = 0f;
            hScore = 0f;
        }
        public void SetParent(AstarNode<T> parent, double gscore)
        {
            this.gScore = gscore;
            this.parent = parent;
        }
        public bool IsNewGScoreBetter(double score)
        {
            return parent == null || score < gScore;
        }

        public int CompareTo(AstarNode<T> other)
        {
            if (FScore < other.FScore)
                return 1;
            if (FScore > other.FScore)
                return -1;
            return 0;
        }
    }
}
