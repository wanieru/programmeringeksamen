﻿using Graphs.Graph;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Graphs.Algorithm.Astar
{
    /// <summary>
    /// Provides Astar algorithm to find path on a graph
    /// </summary>
    public static class Astar
    {
        /// <summary>
        /// Returns a stack of type T of the path towards the goal
        /// </summary>
        /// <typeparam name="T">The type of objects to pathfind between</typeparam>
        /// <param name="graph">The graph to search</param>
        /// <param name="start">The T object to start on</param>
        /// <param name="end">The T object to find</param>
        /// <returns></returns>
        public static Stack<T> FindPath<T>(Graph<T> graph, T start, T end) where T : IPathfindingNode<T>
        {
            //Create a map of Astar specific node representations and Graph nodes
            Dictionary<Node<T>, AstarNode<T>> nodes = new Dictionary<Node<T>, AstarNode<T>>();
            foreach (var node in graph.Nodes)
            {
                nodes.Add(node, new AstarNode<T>(node));
            }

            //Create an open and closed list
            List<AstarNode<T>> openNodes = new List<AstarNode<T>>();
            List<AstarNode<T>> closedNodes = new List<AstarNode<T>>();

            //Add the start nod to the open list
            openNodes.Add(nodes[graph.GetNode(start)]);

            //Run as long as there are open nods or until the end goal has been found
            while (openNodes.Count > 0 && !closedNodes.Contains(nodes[graph.GetNode(end)]))
            {
                //Our current node is the one with the lowest fscore.
                AstarNode<T> currentNode = null;
                foreach (var node in openNodes)
                {
                    if (currentNode == null || node.FScore < currentNode.FScore)
                        currentNode = node;
                }

                var currentNodeContent = currentNode.node.Content; //Its position

                //Then we move it to the closed nodes list.
                openNodes.Remove(currentNode);
                closedNodes.Add(currentNode);

                //Check all nodes
                foreach (var edge in currentNode.node.Edges)
                {
                    if (!edge.ToNode.Content.IsWalkable()) //Skip unwalkable
                        continue;
                    var toNode = nodes[edge.ToNode];
                    if (closedNodes.Contains(toNode)) //Skip closed neighbours
                        continue;
                    var toContent = toNode.node.Content; //Get its positoin
                    if (toNode.IsNewGScoreBetter(currentNodeContent.DistanceTo(toContent))) //Will this new gScore be better for this node?
                    {
                        //If so, update its parent.
                        toNode.SetParent(currentNode, edge.Cost);
                        if (!openNodes.Contains(toNode))
                        {
                            //If it's not alreayd in the open list, calculate its hscore and add it.
                            toNode.hScore =  toContent.DistanceTo(end);
                            openNodes.Add(toNode);
                        }
                    }
                }
            }
            if (!closedNodes.Contains(nodes[graph.GetNode(end)]))
                return null; //No path was found :(

            //Create the stack to go to the path!
            Stack<T> returnStack = new Stack<T>();
            AstarNode<T> pathNode = nodes[graph.GetNode(end)];
            returnStack.Push(pathNode.node.Content);
            while (pathNode.parent != null)
            {
                pathNode = pathNode.parent;
                returnStack.Push(pathNode.node.Content);
            }
            return returnStack;
        }
    }
}