﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphs.Algorithm
{
    /// <summary>
    /// Implement this on the content type for your graph if you want to use astar.
    /// </summary>
    public interface IPathfindingNode<T>
    {
        /// <summary>
        /// The distance between any two nodes in the graph
        /// </summary>
        double DistanceTo(T b);

        /// <summary>
        /// Whether or not this node is walkable
        /// </summary>
        bool IsWalkable();
    }
}
