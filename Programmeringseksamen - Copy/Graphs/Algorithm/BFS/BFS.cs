﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Graphs.Graph;
using Graphs.Algorithm.Astar;

namespace Graphs.Algorithm.BFS
{
    public static class BFS
    {
        public static Stack<T> FindPath<T>(Graph<T> graph, T start, T end) where T : IPathfindingNode<T>
        {
            Queue<Edge<T>> edgesToVisit = new Queue<Edge<T>>();
            edgesToVisit.Enqueue(new Edge<T>(graph.GetNode(start), graph.GetNode(start), 0f));
            Dictionary<Node<T>, Node<T>> parents = new Dictionary<Node<T>, Node<T>>();

            while (edgesToVisit.Count > 0)
            {
                Edge<T> currentEdge = edgesToVisit.Dequeue();
                foreach (Edge<T> edge in currentEdge.ToNode.Edges)
                {
                    if (!edge.ToNode.Content.IsWalkable())
                        continue;
                    if (!parents.ContainsKey(edge.ToNode))
                    {
                        edgesToVisit.Enqueue(edge);
                        parents.Add(edge.ToNode, edge.FromNode);
                        if (edge.ToNode == graph.GetNode(end))
                        {
                            edgesToVisit.Clear();
                            break;
                        }
                    }
                }
            }

            Stack<T> returnStack = new Stack<T>();
            Node<T> currentNode = graph.GetNode(end);
            returnStack.Push(currentNode.Content);

            while (currentNode != graph.GetNode(start))
            {
                currentNode = parents[currentNode];
                returnStack.Push(currentNode.Content);
            }
            return returnStack;
            
        }
    }
}
