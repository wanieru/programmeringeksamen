﻿using Graphs.Algorithm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphs
{
    class ConcreteNode : IPathfindingNode<ConcreteNode>
    {
        private bool isWalkable;
        public int X{ get; private set; }
        public int Y { get; private set; }
        public ConcreteNode(int x, int y, bool isWalkable)
        {
            this.isWalkable = isWalkable;
            this.X = x;
            this.Y = y;
        }
        public bool IsWalkable()
        {
            return isWalkable;
        }

        public double DistanceTo(ConcreteNode b)
        {
            return Math.Abs(X - b.X) + Math.Abs(Y - b.Y);
        }
    }
}
