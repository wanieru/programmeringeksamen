﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DeadLock
{
    class Program
    {
        static object lock1 = new object();
        static object lock2 = new object();
        static void Main(string[] args)
        {
            Thread thread1 = new Thread(Thread1);
            Thread thread2 = new Thread(Thread2);
            thread1.Start();
            thread2.Start();
            Console.ReadLine();
        }
        static void Thread1()
        {
            while (true)
            {
                lock (lock1)
                {
                    lock (lock2)
                    {
                        Console.WriteLine("Greetings from thread 1");
                    }
                }
            }
        }
        static void Thread2()
        {
            while (true)
            {
                lock (lock2)
                {
                    lock (lock1)
                    {
                        Console.WriteLine("Greetings from thread 2");
                    }
                }
            }
        }
    }
}
