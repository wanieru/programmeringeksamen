﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern
{
    class DanishFactory : LanguageFactory
    {
        public override Language ConstructLanguage()
        {
            return new Danish();
        }
    }
}
