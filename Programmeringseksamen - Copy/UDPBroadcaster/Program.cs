﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UDPBroadcaster
{
    class Program
    {
        static void Main(string[] args)
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);

            IPEndPoint endPoint = new IPEndPoint(IPAddress.Broadcast, 10000); //255.255.255.255
            string message = "hewwo are you there?";
            byte[] bytes = Encoding.ASCII.GetBytes("hewwo are you there?");
            while(true)
            {
                socket.SendTo(bytes, endPoint);
                Console.WriteLine("Sent: " + message);
                Thread.Sleep(1000);
            }
        }
    }
}
