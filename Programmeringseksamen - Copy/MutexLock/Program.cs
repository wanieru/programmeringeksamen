﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MutexLock
{
    class Program
    {
        static Mutex mutex = new Mutex();
        static int shared = 0;
        static void Main(string[] args)
        {
            for (var i = 0; i < 100; i++)
            {
                Thread thread = new Thread(ThreadedCode);
                thread.Start();
            }
            Console.ReadLine();
        }
        static void ThreadedCode()
        {
            while (true)
            {
                mutex.WaitOne();
                if (shared == 0)
                {
                    shared++;
                    int division = 10 / shared;
                }
                shared = 0;
                mutex.ReleaseMutex();
            }
        }
    }
}
