﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingletonExample
{
    class Singleton
    {
        private static Singleton i;
        public static Singleton I
        {
            get
            {
                if(i == null)
                {
                    i = new Singleton("Singleton");
                }
                return i;
            }
        }
        public string name;
        private Singleton(string name)
        {
            this.name = name;
        }
    }
}
