﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    abstract class PersonBuilder
    {
        protected Person person;
        public PersonBuilder()
        {
            person = new Person();
        }
        protected abstract void BuildHead();
        protected abstract void BuildTorso();
        protected abstract void BuildLegs();
        public Person GetPerson()
        {
            BuildHead();
            BuildTorso();
            BuildLegs();
            return person;
        }
    }
}
