﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    class QueenElizabethBuilder : PersonBuilder
    {
        protected override void BuildHead()
        {
            person.Head = "She has a crown";
        }

        protected override void BuildLegs()
        {
            person.Legs = "She's short";
        }

        protected override void BuildTorso()
        {
            person.Torso = "She's wearing a green dress";
        }
    }
}
