﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    class EmperorAkihitoBuilder : PersonBuilder
    {
        protected override void BuildHead()
        {
            person.Head = "He has grey hair";
        }

        protected override void BuildLegs()
        {
            person.Legs = "He's short";
        }

        protected override void BuildTorso()
        {
            person.Torso = "He's wearing a suit";
        }
    }
}
