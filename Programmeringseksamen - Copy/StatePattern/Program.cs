﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StatePattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Werewolf werewolf = new Werewolf();
            while(true)
            {
                werewolf.DoAction();
                Thread.Sleep(500);
            }
        }
    }
}
