﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StatePattern
{
    class WerewolfState : IState
    {
        public void DoAction(Werewolf werewolf)
        {
            Console.WriteLine("Werewolf action");
            if (!werewolf.Fullmoon)
            {
                werewolf.ChangeState(new HumanState());
            }
        }
    }
}
