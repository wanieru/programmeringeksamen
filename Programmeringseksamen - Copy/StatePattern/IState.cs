﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StatePattern
{
    interface IState
    {
        void DoAction(Werewolf werewolf);

    }
}
