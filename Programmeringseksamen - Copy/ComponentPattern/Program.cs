﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Directory root = new Directory("C:");
            File hiberfilsys = new File("hiberfil.sys", 500);
            root.Add(hiberfilsys);
            Directory programFiles = new Directory("Program Files");
            root.Add(programFiles);
            Directory visualStudio = new Directory("Visual Studio");
            programFiles.Add(visualStudio);
            File vsexe = new File("vs.exe", 10);
            visualStudio.Add(vsexe);
            Directory users = new Directory("Users");
            root.Add(users);
            File document1 = new File("Document 1", 2);
            users.Add(document1);
            File document2 = new File("Document 2", 5);
            users.Add(document2);

            root.PrintString(0);
            Console.ReadLine();
        }
    }
}
