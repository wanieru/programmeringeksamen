﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentPattern
{
    abstract class FileSystemComponent
    {
        protected void PrintTabs(int count)
        {
            for (int i = 0; i < count; i++)
            {
                Console.Write("\t");
            }
        }
        public abstract void PrintString(int indent);
    }
}
