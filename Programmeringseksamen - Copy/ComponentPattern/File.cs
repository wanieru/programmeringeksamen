﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentPattern
{
    class File : FileSystemComponent
    {
        private string name;
        private int size;
        public File(string name, int size)
        {
            this.name = name;
            this.size = size;
        }

        public override void PrintString(int indent)
        {
            PrintTabs(indent);
            Console.WriteLine(name + " (" + size + " MB)");
        }
    }
}
