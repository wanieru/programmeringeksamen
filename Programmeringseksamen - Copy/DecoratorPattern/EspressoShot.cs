﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern
{
    class EspressoShot : CoffeeIngredient
    {
        public EspressoShot(Coffee decoratedCoffee) : base(decoratedCoffee) { }
        public override int Cost
        {
            get
            {
                return 10 + decoratedCoffee.Cost;
            }
        }

        public override void PrintIngredients()
        {
            decoratedCoffee.PrintIngredients();
            Console.WriteLine("Espresso Shot");
        }
    }
}
