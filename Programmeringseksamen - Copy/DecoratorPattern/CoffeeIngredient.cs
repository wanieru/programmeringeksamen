﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern
{
    abstract class CoffeeIngredient : Coffee
    {
        protected Coffee decoratedCoffee;
        protected CoffeeIngredient(Coffee decoratedCoffee)
        {
            this.decoratedCoffee = decoratedCoffee;
        }
    }
}
