﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Coffee CafeLatte = new Milk(new Milk(new Milk(new BaseCoffee())));
            Coffee Bitter = new EspressoShot(new EspressoShot(new BaseCoffee()));
            Coffee Deluxe = new WhippedCream(new ChocolateSauce(new Milk(new BaseCoffee())));

            Console.WriteLine("CafeLatté: " + CafeLatte.Cost);
            CafeLatte.PrintIngredients();
            Console.WriteLine();

            Console.WriteLine("Bitter: " + Bitter.Cost);
            Bitter.PrintIngredients();
            Console.WriteLine();

            Console.WriteLine("Deluxe: " + Deluxe.Cost);
            Deluxe.PrintIngredients();
            Console.WriteLine();

            Console.ReadLine();
        }
    }
}
