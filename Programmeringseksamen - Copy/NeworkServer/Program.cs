﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NeworkServer
{
    class Program
    {
        private static TcpListener listener;
        private static int port = 10000;
        private static List<NetworkStream> clients = new List<NetworkStream>();
        private static int messageNumber;
        private static void Main(string[] args)
        {
            listener = new TcpListener(IPAddress.Any, port);
            listener.Start();
            Console.WriteLine("Server running");

            Thread thread = new Thread(SendMessage);
            thread.Start();

            while(true)
            {
                Console.WriteLine("Ready to accept a client...");
                TcpClient client = listener.AcceptTcpClient();
                Console.WriteLine("Accepted a client!");
                clients.Add(client.GetStream());
            }
        }
        private static void SendMessage()
        {
            while(true)
            {
                Thread.Sleep(1000);
                messageNumber++;
                string message = (DateTime.Now.ToString()) + ": " + messageNumber;
                Console.WriteLine("Sending message: " + message);
                foreach(var client in clients)
                {
                    try
                    {
                        byte[] bytesToSend = Encoding.UTF8.GetBytes(message);
                        client.Write(BitConverter.GetBytes(bytesToSend.Length), 0, 4);
                        client.Write(bytesToSend, 0, bytesToSend.Length);
                    }
                    catch (IOException exception)
                    {
                        Console.WriteLine(exception.Message);
                    }
                    catch(ObjectDisposedException exception)
                    {
                        Console.WriteLine(exception.Message);
                    }
                }
            }
        }
    }
}
