﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoolPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            const int SIMULTANEOUS_OBJECTS = 50;
            const int CYCLES = 10000;

            DateTime poolBefore = DateTime.Now;

            Pool<ConcretePoolObject> pool = new Pool<ConcretePoolObject>();
            Stack<ConcretePoolObject> createdObjects = new Stack<ConcretePoolObject>();
            for (int cycle = 0; cycle < CYCLES; cycle++)
            {
                for (int obj = 0; obj < SIMULTANEOUS_OBJECTS; obj++)
                {
                    createdObjects.Push(pool.Get());
                }
                while(createdObjects.Count > 0)
                {
                    pool.Release(createdObjects.Pop());
                }
            }

            DateTime poolAfter = DateTime.Now;

            for (int cycle = 0; cycle < CYCLES; cycle++)
            {
                for (int obj = 0; obj < SIMULTANEOUS_OBJECTS; obj++)
                {
                    ConcretePoolObject newObject = new ConcretePoolObject();
                }
            }

            DateTime nonPoolAfter = DateTime.Now;

            Console.WriteLine("Pool: " + (poolAfter - poolBefore).TotalMilliseconds + "ms");
            Console.WriteLine("Nonpool: " + (nonPoolAfter - poolAfter).TotalMilliseconds + "ms");
            Console.ReadLine();
        }
    }
}
