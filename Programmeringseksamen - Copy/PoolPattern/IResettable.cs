﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoolPattern
{
    interface IResettable
    {
        void Reset();
    }
}
