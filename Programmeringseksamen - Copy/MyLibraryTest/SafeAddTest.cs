﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLibraryTest
{
    [TestClass]
    public class SafeAddTest
    {
        [TestMethod]
        [ExpectedException(typeof(OverflowException))]
        public void SafeAddTest_MinValue_Plus_Neg1_Equals_OverflowException()
        {
            LibraryClass.SafeAdd(int.MinValue, -1);
        }

        [TestMethod]
        [ExpectedException(typeof(OverflowException))]
        public void SafeAddTest_MaxValue_Plus_1_Equals_OverflowException()
        {
            LibraryClass.SafeAdd(int.MaxValue, 1);
        }

        [TestMethod]
        public void SafeAddTest_Neg10_Plus_30_Equals_20()
        {
            Assert.AreEqual(LibraryClass.SafeAdd(-10, 30), 20);
        }

        [TestMethod]
        public void SafeAddTest_MinValue_Plus_MaxValue_Equals_Neg1()
        {
            Assert.AreEqual(LibraryClass.SafeAdd(int.MinValue, int.MaxValue), -1);
        }
    }
}
