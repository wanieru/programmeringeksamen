﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace UDPListener
{
    class Program
    {
        static void Main(string[] args)
        {
            UdpClient receiver = new UdpClient(10000);
            while (true)
            {
                IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, 10000);
                byte[] message = receiver.Receive(ref endPoint);
                string receivedMessage = Encoding.ASCII.GetString(message);
                Console.WriteLine("Received: "+receivedMessage);
            }
        }
    }
}
