﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BubbleSort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new[] { 6, 2, 0, 6, 2, 7, 9, 10, 591, -65 };
            PrintArray(array);
            BubbleSort(array);
            PrintArray(array);
            Console.ReadLine();
        }

        static void BubbleSort<T>(T[] array) where T : IComparable<T>
        {
            bool swapped = false;
            do
            {
                swapped = false;
                for (int i = 1; i < array.Length; i++)
                {
                    if(array[i].CompareTo(array[i-1]) < 0)
                    {
                        Swap(array, i, i - 1);
                        swapped = true;
                    }
                }
            } while (swapped);
        }
        static void Swap<T>(T[] array, int a, int b)
        {
            T aValue = array[a];
            array[a] = array[b];
            array[b] = aValue;
        }

        static void PrintArray<T>(T[] array)
        {
            foreach (T item in array)
            {
                Console.Write(item.ToString() + ", ");
            }
            Console.WriteLine();
        }
    }
}
