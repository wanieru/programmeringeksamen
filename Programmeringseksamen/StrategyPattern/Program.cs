﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Mathematician mathematician = new Mathematician();
            IStrategy[] strategies = new IStrategy[]
            {
                new Add(),
                new Multiply(),
                new Subtract()
            };
            foreach(IStrategy strategy in strategies)
            {
                mathematician.Strategy = strategy;
                Console.WriteLine("New strategy: " + strategy);
                mathematician.ProcessNumber(5, 10);
                mathematician.ProcessNumber(50, 150);
                mathematician.ProcessNumber(200, 0);
            }
            Console.ReadLine();
        }
    }
}
