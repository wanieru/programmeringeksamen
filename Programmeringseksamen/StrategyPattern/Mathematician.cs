﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyPattern
{
    class Mathematician
    {
        public IStrategy Strategy { get; set; }
        public void ProcessNumber(int a, int b)
        {
            Console.WriteLine("Doing operation on " + a + " and " + b + " = " + Strategy.DoOperation(a, b));
        }
    }
}
