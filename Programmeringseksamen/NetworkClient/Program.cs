﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace NetworkClient
{
    class Program
    {
        static void Main(string[] args)
        {
            int port = 10000;
            TcpClient client = new TcpClient("localhost", port);
            NetworkStream stream = client.GetStream();
            while (true)
            {
                byte[] sizeBytes = new byte[4];
                stream.Read(sizeBytes, 0, 4);
                int length = BitConverter.ToInt32(sizeBytes, 0);
                byte[] messageBytes = new byte[length];
                stream.Read(messageBytes, 0, messageBytes.Length);
                string message = Encoding.UTF8.GetString(messageBytes);
                Console.WriteLine("Received message: " + message);
            }
        }
    }
}
