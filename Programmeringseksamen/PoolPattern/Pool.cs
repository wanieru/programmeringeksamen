﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoolPattern
{
    class Pool<T> where T : IResettable, new()
    {
        private Queue<T> pool;
        public Pool()
        {
            pool = new Queue<T>();
        }
        public T Get()
        {
            if(pool.Count < 1)
            {
                return new T();
            }
            return pool.Dequeue();
        }
        public void Release(T obj)
        {
            obj.Reset();
            pool.Enqueue(obj);
        }
    }
}
