﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PoolPattern
{
    class ConcretePoolObject : IResettable
    {
        private int health;
        private string day;
        public ConcretePoolObject()
        {
            //Simulate loading from a file
            day = DateTime.Now.Day.ToString();
            health = 100;
        }
        public void Reset()
        {
            health = 100;
        }
    }
}
