﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StatePattern
{
    class HumanState : IState
    {
        public void DoAction(Werewolf werewolf)
        {
            Console.WriteLine("Human action");
            if(werewolf.Fullmoon)
            {
                werewolf.ChangeState(new WerewolfState());
            }
        }
    }
}
