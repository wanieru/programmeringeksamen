﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StatePattern
{
    class Werewolf
    {
        private Random random = new Random();
        private IState state = new HumanState();
        public bool Fullmoon { get; private set; } = false;

        public void DoAction()
        {
            if(random.Next(0, 11) == 0)
            {
                Fullmoon = !Fullmoon;
                Console.WriteLine("Fullmoon is now " + Fullmoon);
            }
            state.DoAction(this);
        }
        public void ChangeState(IState state)
        {
            this.state = state;
            Console.WriteLine("Changed state to " + state.ToString());
        }
    }
}
