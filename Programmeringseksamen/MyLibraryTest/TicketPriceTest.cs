﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLibraryTest
{
    [TestClass]
    public class TicketPriceTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TicketPrice_AgeMinValue_ArgumentException()
        {
            LibraryClass.CalculateTicketPrice(int.MinValue);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TicketPrice_AgeNeg1_ArgumentException()
        {
            LibraryClass.CalculateTicketPrice(-1);
        }

        [TestMethod]
        public void TicketPrice_Age0_0()
        {
            Assert.AreEqual(LibraryClass.CalculateTicketPrice(0), 0);
        }

        [TestMethod]
        public void TicketPrice_Age2_0()
        {
            Assert.AreEqual(LibraryClass.CalculateTicketPrice(2), 0);
        }

        [TestMethod]
        public void TicketPrice_Age3_5()
        {
            Assert.AreEqual(LibraryClass.CalculateTicketPrice(3), 5);
        }

        [TestMethod]
        public void TicketPrice_Age9_5()
        {
            Assert.AreEqual(LibraryClass.CalculateTicketPrice(9), 5);
        }

        [TestMethod]
        public void TicketPrice_Age10_15()
        {
            Assert.AreEqual(LibraryClass.CalculateTicketPrice(10), 15);
        }

        [TestMethod]
        public void TicketPrice_Age17_15()
        {
            Assert.AreEqual(LibraryClass.CalculateTicketPrice(17), 15);
        }

        [TestMethod]
        public void TicketPrice_Age18_20()
        {
            Assert.AreEqual(LibraryClass.CalculateTicketPrice(18), 20);
        }

        [TestMethod]
        public void TicketPrice_Age64_20()
        {
            Assert.AreEqual(LibraryClass.CalculateTicketPrice(64), 20);
        }

        [TestMethod]
        public void TicketPrice_Age65_10()
        {
            Assert.AreEqual(LibraryClass.CalculateTicketPrice(65), 10);
        }

        [TestMethod]
        public void TicketPrice_AgeMaxValue_10()
        {
            Assert.AreEqual(LibraryClass.CalculateTicketPrice(int.MaxValue), 10);
        }
    }
}
