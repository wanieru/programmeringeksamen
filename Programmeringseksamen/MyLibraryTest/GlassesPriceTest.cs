﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyLibrary;

namespace MyLibraryTest
{
    [TestClass]
    public class GlassesPriceTest
    {
        [TestMethod]
        public void GlassesPrice_MemberAge25_4()
        {
            Assert.AreEqual(LibraryClass.GlassesPrice(25, true), 4);
        }

        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException))]
        public void GlassesPrice_MemberAge0_DivideByZeroException()
        {
            LibraryClass.GlassesPrice(0, true);
        }

        [TestMethod]
        public void GlassesPrice_MemberAge100_1()
        {
            Assert.AreEqual(LibraryClass.GlassesPrice(100, true), 1);
        }

        [TestMethod]
        public void GlassesPrice_NonMemberAge100_100()
        {
            Assert.AreEqual(LibraryClass.GlassesPrice(100, false), 100);
        }

        [TestMethod]
        public void GlassesPrice_NonMemberAge60_200()
        {
            Assert.AreEqual(LibraryClass.GlassesPrice(60, false), 200);
        }

        [TestMethod]
        public void GlassesPrice_NonMemberAge25_8()
        {
            Assert.AreEqual(LibraryClass.GlassesPrice(25, false), 8);
        }

        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException))]
        public void GlassesPrice_NonMemberAge0_DivideByZeroException()
        {
            LibraryClass.GlassesPrice(0, false);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void GlassesPrice_NonMemberMinValue_ArgumentException()
        {
            LibraryClass.GlassesPrice(int.MinValue, false);
        }
        [TestMethod]
        public void GlassesPrice_MemberMaxValue_0()
        {
            Assert.AreEqual(LibraryClass.GlassesPrice(int.MaxValue, true), 0);
        }
    }
}
