﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            PersonBuilder emperorAkihitoBuilder = new EmperorAkihitoBuilder();
            PersonBuilder queenElizabethBuilder = new QueenElizabethBuilder();

            Person emperorAkihito = emperorAkihitoBuilder.GetPerson();
            Person queenElizabeth = queenElizabethBuilder.GetPerson();

            Console.WriteLine(emperorAkihito.GetDescription());
            Console.WriteLine(queenElizabeth.GetDescription());

            Console.ReadLine();
        }
    }
}
