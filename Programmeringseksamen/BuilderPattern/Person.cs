﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderPattern
{
    class Person
    {
        public string Head { private get; set; }
        public string Torso { private get; set; }
        public string Legs { private get; set; }
        public string GetDescription()
        {
            return "Head: " + Head + ", Torso: " + Torso + ", Legs: " + Legs;
        }
    }
}
