﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quicksort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new[] { 6, 2, 0, 6, 2, 7, 9, 10, 591, -65 };
            PrintArray(array);
            QuickSort(array);
            PrintArray(array);
            Console.ReadLine();
        }

        static void QuickSort<T>(T[] array, int left = -1, int right = -1) where T : IComparable<T>
        {
            if (left < 0)
            {
                left = 0;
            }
            if (right < 0)
            {
                right = array.Length - 1;
            }
            if (left < right)
            {
                int pivot = Partition(array, left, right);
                QuickSort(array, left, pivot - 1);
                QuickSort(array, pivot + 1, right);
            }
        }
        static int Partition<T>(T[] array, int left, int right) where T : IComparable<T>
        {
            T pivotValue = array[right];
            int storeIndex = left;
            for (int i = left; i <= right; i++)
            {
                if(array[i].CompareTo(pivotValue) < 0)
                {
                    Swap(array, i, storeIndex);
                    storeIndex++;
                }
            }
            Swap(array, storeIndex, right);
            return storeIndex;
        }

        static void Swap<T>(T[] array, int a, int b)
        {
            if (a == b) return;
            T aValue = array[a];
            array[a] = array[b];
            array[b] = aValue;
        }

        static void PrintArray<T>(T[] array)
        {
            foreach (T item in array)
            {
                Console.Write(item.ToString() + ", ");
            }
            Console.WriteLine();
        }
    }
}
