﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsertionSort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new[] { 6, 2, 0, 6, 2, 7, 9, 10, 591, -65 };
            PrintArray(array);
            InsertionSort(array);
            PrintArray(array);
            Console.ReadLine();
        }

        static void InsertionSort<T>(T[] array) where T : IComparable<T>
        {
            for(int i=1; i < array.Length;i++)
            {
                T value = array[i];
                int pointer = i;
                while(pointer > 0 && value.CompareTo(array[pointer - 1]) < 0)
                {
                    array[pointer] = array[pointer - 1];
                    pointer--;
                }
                array[pointer] = value;
            }
        }
        static void PrintArray<T>(T[] array)
        {
            foreach(T item in array)
            {
                Console.Write(item.ToString() + ", ");
            }
            Console.WriteLine();
        }
    }
}
