﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentPattern
{
    class Directory : FileSystemComposite
    {
        private string name;
        public Directory(string name)
        {
            this.name = name;
        }
        public override void PrintString(int indent)
        {
            PrintTabs(indent);
            Console.WriteLine(name + "/");
            foreach(FileSystemComponent component in components)
            {
                component.PrintString(indent + 1);
            }
        }
    }
}
