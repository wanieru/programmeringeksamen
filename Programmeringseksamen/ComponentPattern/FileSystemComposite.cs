﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentPattern
{
    abstract class FileSystemComposite : FileSystemComponent
    {
        protected List<FileSystemComponent> components = new List<FileSystemComponent>();
        public void Add(FileSystemComponent component)
        {
            components.Add(component);
        }
    }
}
