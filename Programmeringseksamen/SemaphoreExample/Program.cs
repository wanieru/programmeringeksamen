﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SemaphoreExample
{
    class Program
    {
        static Semaphore loadingSemaphore = new Semaphore(3, 3);
        static Random random = new Random();

        static void Main(string[] args)
        {
            for (int i = 0; i < 40; i++)
            {
                new Thread(Load).Start(i);
            }
        }

        public static void Load(object i)
        {
            loadingSemaphore.WaitOne();
            Console.WriteLine(i + " start loading.");
            Thread.Sleep(random.Next(2000, 5000));
            Console.WriteLine(i + " finished loading.");
            loadingSemaphore.Release();
        }
    }
}
