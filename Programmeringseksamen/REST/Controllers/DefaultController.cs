﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace REST.Controllers
{
    public class DefaultController : ApiController
    {
        private List<string> strings  = new List<string>()
        {
            "value1",
            "value2",
            "value3",
            "value4"
        };
        // GET: api/Default
        public IEnumerable<string> Get()
        {
            return strings;
        }

        // GET: api/Default/5
        public string Get(int id)
        {
            return strings[id];
        }

        // POST: api/Default
        public void Post([FromBody]string value)
        {
            strings.Add(value);
        }

        // PUT: api/Default/5
        public void Put(int id, [FromBody]string value)
        {
            if (strings.Count <= id)
            {
                return;
            }
            strings[id] = value;
        }

        // DELETE: api/Default/5
        public void Delete(int id)
        {
            if(strings.Count <= id)
            {
                return;
            }
            strings.RemoveAt(id);
        }
    }
}
