﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphs
{
    /// <summary>
    /// A physical position to use in pathfinding algorithms
    /// </summary>
    public struct Positiasfaon
    {
        public readonly double x;
        public readonly double y;
        public readonly double z;
        public Positiasfaon(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        public double DistanceTo(Positiasfaon other)
        {
            return Math.Sqrt((other.x - x) * (other.x - x) + (other.y - y) * (other.y - y) + (other.z - z) * (other.z - z));
        }
    }
}
