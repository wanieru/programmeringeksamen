﻿using Graphs.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphs.Algorithm.DFSRandom
{
    public class DFS
    {
        public static Stack<T> FindPath<T>(Graph<T> graph, T start, T end) where T : IPathfindingNode<T>
        {
            Stack<Edge<T>> edgesToVisit = new Stack<Edge<T>>();
            edgesToVisit.Push(new Edge<T>(graph.GetNode(start), graph.GetNode(start), 0f));
            Dictionary<Node<T>, Node<T>> parents = new Dictionary<Node<T>, Node<T>>();

            while (edgesToVisit.Count > 0)
            {
                Edge<T> currentEdge = edgesToVisit.Pop();
                if (!parents.ContainsKey(currentEdge.ToNode))
                {
                    parents.Add(currentEdge.ToNode, currentEdge.FromNode);
                    if (currentEdge.ToNode == graph.GetNode(end))
                    {
                        break;
                    }
                }
                foreach (var edge in currentEdge.ToNode.Edges)
                {
                    if (!edge.ToNode.Content.IsWalkable())
                        continue;
                    if (!parents.ContainsKey(edge.ToNode))
                    {
                        edgesToVisit.Push(edge);
                    }
                }
            }

            Stack<T> returnStack = new Stack<T>();
            Node<T> currentNode = graph.GetNode(end);
            returnStack.Push(currentNode.Content);

            while (currentNode != graph.GetNode(start))
            {
                currentNode = parents[currentNode];
                returnStack.Push(currentNode.Content);
            }
            return returnStack;

        }
    }
}
