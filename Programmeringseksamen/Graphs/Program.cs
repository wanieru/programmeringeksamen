﻿using Graphs.Algorithm.Astar;
using Graphs.Graph;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Graphs
{
    class Program
    {
        private static Random random = new Random();
        static void Main(string[] args)
        {
            while(true)
            {
                Graph<ConcreteNode> graph = CreateGraph();

                Node<ConcreteNode>[] walkableNodes = graph.Nodes.Where(n => n.Content.IsWalkable()).ToArray();
                ConcreteNode start = walkableNodes[random.Next(0, walkableNodes.Length)].Content;
                ConcreteNode end = walkableNodes[random.Next(0, walkableNodes.Length)].Content;

                Stack<ConcreteNode> path = Astar.FindPath(graph, start, end);

                if (path == null)
                    continue;

                DrawColor(start, ConsoleColor.Cyan);
                DrawColor(end, ConsoleColor.Magenta);

                while(path.Count > 0)
                {
                    DrawColor(path.Pop(), ConsoleColor.Green);
                    Thread.Sleep(50);
                }

                Thread.Sleep(500);
            }
        }
        private static Graph<ConcreteNode> CreateGraph()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Clear();


            int width = Console.BufferWidth;
            int height = Console.WindowHeight;

            Node<ConcreteNode>[,] tiles = new Node<ConcreteNode>[width, height];
            Graph<ConcreteNode> graph = new Graph<ConcreteNode>();
            //Add nodes
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    bool isWalkable = random.NextDouble() > 0.25d;
                    ConcreteNode concreteNode = new ConcreteNode(x, y, isWalkable);
                    tiles[x, y] = graph.AddNode(concreteNode);
                    DrawColor(concreteNode, ConsoleColor.Black);
                }
            }
            //Add edges
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    Node<ConcreteNode> from = tiles[x, y];
                    if (x - 1 >= 0)
                    {
                        graph.AddEdge(from, tiles[x - 1, y], false, 1d);
                    }
                    if (y - 1 >= 0)
                    {
                        graph.AddEdge(from, tiles[x, y - 1], false, 1d);
                    }
                    if (x + 1 < width)
                    {
                        graph.AddEdge(from, tiles[x + 1, y], false, 1d);
                    }
                    if (y + 1 < height)
                    {
                        graph.AddEdge(from, tiles[x, y + 1], false, 1d);
                    }
                }
            }
            return graph;
        }
        private static void DrawColor(ConcreteNode node, ConsoleColor walkableColor)
        {
            Console.SetCursorPosition(node.X, node.Y);
            Console.BackgroundColor = node.IsWalkable() ? walkableColor : ConsoleColor.White;
            Console.Write(" ");
        }
    }
}
