﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern
{
    class Danish : Language
    {
        public override string Hello()
        {
            return "Hej";
        }
    }
}
