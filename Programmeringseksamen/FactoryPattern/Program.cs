﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            LanguageFactory[] factories = new LanguageFactory[]
            {
                new SpanishFactory(),
                new DanishFactory()
            };
            foreach(LanguageFactory factory in factories)
            {
                Language language = factory.ConstructLanguage();
                Console.WriteLine(language.Hello());
            }
            Console.ReadLine();
        }
    }
}
