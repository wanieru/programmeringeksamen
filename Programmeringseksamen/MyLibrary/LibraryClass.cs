﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLibrary
{
    public static class LibraryClass
    {
        //Grænseværditest
        public static int CalculateTicketPrice(int age)
        {
            if(age < 0)
            {
                throw new ArgumentException("Invalid age");
            }
            if(age < 3)
            {
                return 0;
            }
            if(age < 10)
            {
                return 5;
            }
            if(age < 18)
            {
                return 15;
            }
            if(age < 65)
            {
                return 20;
            }
            return 10;
        }
        public static int SafeAdd(int a, int b)
        {
            int sum = a + b;
            if(a > 0 && b > 0 && sum < 0)
            {
                throw new OverflowException("Positive overflow");
            }
            if(a < 0 && b < 0 && sum > 0)
            {
                throw new OverflowException("Negative overflow");
            }
            return sum;
        }
        public static int GlassesPrice(int age, bool member)
        {
            if(age < 0)
            {
                throw new ArgumentException("Age cannot be negative");
            }
            int agePrice = 200;
            if(member || age > 75)
            {
                agePrice = 100;
            }
            if(age < 50 || member)
            {
                agePrice /= age;
            }
            return agePrice;
        }
    }
}
