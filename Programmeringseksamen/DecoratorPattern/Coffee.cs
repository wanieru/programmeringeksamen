﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern
{
    abstract class Coffee
    {
        public abstract int Cost { get; }
        public abstract void PrintIngredients();
    }
}
