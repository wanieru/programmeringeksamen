﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern
{
    class BaseCoffee : Coffee
    {
        public override int Cost => 10;

        public override void PrintIngredients()
        {
            Console.WriteLine("Coffee");
        }
    }
}
