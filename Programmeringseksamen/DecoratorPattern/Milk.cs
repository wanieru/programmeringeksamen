﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DecoratorPattern
{
    class Milk : CoffeeIngredient
    {
        public Milk(Coffee decoratedCoffee) : base(decoratedCoffee) { }
        public override int Cost
        {
            get
            {
                return 5 + decoratedCoffee.Cost;
            }
        }

        public override void PrintIngredients()
        {
            decoratedCoffee.PrintIngredients();
            Console.WriteLine("Milk");
        }
    }
}
